object FrPrincipal: TFrPrincipal
  Left = 0
  Top = 0
  ActiveControl = cxButtonIniciar
  Caption = 'Servidor da Aplica'#231#227'o'
  ClientHeight = 242
  ClientWidth = 560
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxButtonIniciar: TcxButton
    Left = 16
    Top = 200
    Width = 75
    Height = 25
    Caption = 'Start Server'
    TabOrder = 0
    OnClick = cxButtonIniciarClick
  end
  object RESTServicePooler: TRESTServicePooler
    Active = False
    CORS = False
    ServicePort = 8082
    ProxyOptions.Port = 8888
    ServerParams.HasAuthentication = True
    ServerParams.UserName = 'admin'
    ServerParams.Password = 'admin'
    SSLMethod = sslvSSLv2
    SSLVersions = []
    Encoding = esUtf8
    ServerContext = 'restdataware'
    RootPath = '/'
    SSLVerifyMode = []
    SSLVerifyDepth = 0
    ForceWelcomeAccess = False
    Left = 72
    Top = 16
  end
end
