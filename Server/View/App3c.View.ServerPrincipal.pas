unit App3c.View.ServerPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uDWAbout, uRESTDWBase, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, Vcl.StdCtrls, cxButtons;

type
  TFrPrincipal = class(TForm)
    RESTServicePooler: TRESTServicePooler;
    cxButtonIniciar: TcxButton;
    procedure cxButtonIniciarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrPrincipal: TFrPrincipal;

implementation

uses
  App3c.Connection.DMServer;

{$R *.dfm}

procedure TFrPrincipal.cxButtonIniciarClick(Sender: TObject);
begin
  RESTServicePooler.Active := (not RESTServicePooler.Active);

  if (RESTServicePooler.Active) then
    cxButtonIniciar.Caption := 'Stop Server'
  else
    cxButtonIniciar.Caption := 'Start Server';

end;

procedure TFrPrincipal.FormCreate(Sender: TObject);
begin
  RESTServicePooler.ServerMethodClass := TDMServer;
end;

end.
