object DMServer: TDMServer
  OldCreateOrder = False
  Encoding = esASCII
  Height = 283
  Width = 399
  object FDConnection: TFDConnection
    Params.Strings = (
      
        'Database=C:\Program Files\Firebird\Firebird_3_0\examples\empbuil' +
        'd\EMPLOYEE.FDB'
      'User_Name=sysdba'
      'Password=153'
      'DriverID=FB')
    ConnectedStoredUsage = []
    LoginPrompt = False
    Left = 64
    Top = 32
  end
  object RESTDWDriverFD: TRESTDWDriverFD
    CommitRecords = 100
    Connection = FDConnection
    Left = 64
    Top = 88
  end
  object RESTDWPoolerDB: TRESTDWPoolerDB
    RESTDriver = RESTDWDriverFD
    Compression = True
    Encoding = esUtf8
    StrsTrim = False
    StrsEmpty2Null = False
    StrsTrim2Len = True
    Active = True
    PoolerOffMessage = 'RESTPooler not active.'
    ParamCreate = True
    Left = 64
    Top = 144
  end
  object FDPhysFBDriverLink: TFDPhysFBDriverLink
    Left = 176
    Top = 32
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 288
    Top = 32
  end
end
