unit App3c.Connection.DMServer;

interface

uses
  uDWDataModule,
  System.Classes,
  System.SysUtils, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait, FireDAC.Comp.UI, FireDAC.Phys.IBBase, uRESTDWPoolerDB,
  uDWAbout, uRestDWDriverFD, Data.DB, FireDAC.Comp.Client;

type
  TDMServer = class(TServerMethodDataModule)
    FDConnection: TFDConnection;
    RESTDWDriverFD: TRESTDWDriverFD;
    RESTDWPoolerDB: TRESTDWPoolerDB;
    FDPhysFBDriverLink: TFDPhysFBDriverLink;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMServer: TDMServer;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
