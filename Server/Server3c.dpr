program Server3c;

uses
  Vcl.Forms,
  App3c.View.ServerPrincipal in 'View\App3c.View.ServerPrincipal.pas' {FrPrincipal},
  App3c.Connection.DMServer in 'Connection\App3c.Connection.DMServer.pas' {DMServer: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrPrincipal, FrPrincipal);
  Application.Run;
end.
