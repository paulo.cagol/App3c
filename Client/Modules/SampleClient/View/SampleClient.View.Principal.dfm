object FrPrincipal: TFrPrincipal
  Left = 0
  Top = 0
  Caption = 'Sample Client - App3c'
  ClientHeight = 460
  ClientWidth = 845
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 845
    Height = 57
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object cxButtonOpenClose: TcxButton
      Left = 8
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Open/Close'
      TabOrder = 0
      OnClick = cxButtonOpenCloseClick
    end
    object cxDBNavigator1: TcxDBNavigator
      Left = 104
      Top = 16
      Width = 270
      Height = 25
      Buttons.CustomButtons = <>
      DataSource = DataSource
      TabOrder = 1
    end
  end
  object DBGridLista: TDBGrid
    Left = 0
    Top = 57
    Width = 845
    Height = 403
    Align = alClient
    DataSource = DataSource
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object RESTDWClientSQL: TRESTDWClientSQL
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    MasterCascadeDelete = True
    Datapacks = -1
    DataCache = False
    Params = <>
    DataBase = DMClientConnection.RESTDWDataBase
    SQL.Strings = (
      'SELECT *  FROM PRODUTO')
    UpdateTableName = 'PRODUTO'
    CacheUpdateRecords = True
    AutoCommitData = False
    AutoRefreshAfterCommit = False
    RaiseErrors = True
    ActionCursor = crSQLWait
    ReflectChanges = False
    Left = 56
    Top = 184
    object RESTDWClientSQLID: TIntegerField
      DisplayLabel = 'Cod.'
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object RESTDWClientSQLDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      DisplayWidth = 112
      FieldName = 'DESCRICAO'
      Size = 200
    end
    object RESTDWClientSQLVALORUNITARIO: TFloatField
      DisplayLabel = 'Valor Unit'#225'rio'
      FieldName = 'VALORUNITARIO'
      Required = True
    end
  end
  object DataSource: TDataSource
    DataSet = RESTDWClientSQL
    Left = 56
    Top = 240
  end
end
