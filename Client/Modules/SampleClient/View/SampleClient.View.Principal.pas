unit SampleClient.View.Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, uDWConstsData, uRESTDWPoolerDB, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, FireDAC.Stan.StorageBin, cxControls, cxNavigator,
  cxDBNavigator;

type
  TFrPrincipal = class(TForm)
    Panel1: TPanel;
    DBGridLista: TDBGrid;
    RESTDWClientSQL: TRESTDWClientSQL;
    DataSource: TDataSource;
    RESTDWClientSQLVALORUNITARIO: TFloatField;
    RESTDWClientSQLDESCRICAO: TStringField;
    RESTDWClientSQLID: TIntegerField;
    cxButtonOpenClose: TcxButton;
    cxDBNavigator1: TcxDBNavigator;
    procedure cxButtonOpenCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrPrincipal: TFrPrincipal;

implementation

{$R *.dfm}

uses
  App3c.DM.ClientConnection;

procedure TFrPrincipal.cxButtonOpenCloseClick(Sender: TObject);
begin
  RESTDWClientSQL.Active := (not RESTDWClientSQL.Active);
end;

procedure TFrPrincipal.FormCreate(Sender: TObject);
begin
  if (not Assigned(DMClientConnection)) then
    Application.CreateForm(TDMClientConnection, DMClientConnection);
end;

end.
