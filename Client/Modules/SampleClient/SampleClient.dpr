program SampleClient;

uses
  Vcl.Forms,
  SampleClient.View.Principal in 'View\SampleClient.View.Principal.pas' {FrPrincipal},
  App3c.DM.ClientConnection in '..\..\Commons\ClientDMConnection\App3c.DM.ClientConnection.pas' {DMClientConnection: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrPrincipal, FrPrincipal);
  Application.Run;
end.
