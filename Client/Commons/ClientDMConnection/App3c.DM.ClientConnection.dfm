object DMClientConnection: TDMClientConnection
  OldCreateOrder = False
  Height = 317
  Width = 564
  object RESTDWDataBase: TRESTDWDataBase
    Active = False
    Compression = True
    Login = 'admin'
    Password = 'admin'
    Proxy = False
    ProxyOptions.Port = 8888
    PoolerService = '127.0.0.1'
    PoolerPort = 8082
    PoolerName = 'TDMServer.RESTDWPoolerDB'
    StateConnection.AutoCheck = False
    StateConnection.InTime = 1000
    RequestTimeOut = 10000
    EncodeStrings = True
    Encoding = esUtf8
    StrsTrim = False
    StrsEmpty2Null = False
    StrsTrim2Len = True
    ParamCreate = True
    ClientConnectionDefs.Active = False
    Left = 40
    Top = 256
  end
end
